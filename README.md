# pretix2venueless

This is a simple script to create venueless accounts based off pretix tickets.

# Flow

1. [x] Fetch tickets off pretix
   1. [x] Persist date to file, fetch only those created after previously stored date
   1. [x] Pluck relevant information
1. [x] For each Ticket:
   1. [x] Generate JWT + access ID for venueless
   1. [x] Write JWT into database
   1. [x] Send email (grouped by recipient)

## Configuration

### Environment

You need to create a `.env` file with the following content (adjust accordingly):

```shell
# set to 1 to use mock data and not touch the database or smtp server
P2V_DRY=0
# set any of the below to 1 to not touch the respective service
P2V_DRY_PRETIX=0
P2V_DRY_VENUELESS=0
P2V_DRY_SMTP=0

PRETIX_URL=https://pretix.my-domain.tld/api/
PRETIX_AUTH_TOKEN=aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa
PRETIX_ORGANIZER=my-pretix-org
PRETIX_EVENT=my-pretix-event
PRETIX_ANSWER_NAME=666
# comma-separated list of order IDs; keep empty to not apply any filter
PRETIX_ORDER_FILTER=AAAAA,BBBBB

JWT_SECRET=bbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbb
JWT_ISS=https://venueless.my-domain.tld
JWT_AUD=my-venueless-event

PGHOST=localhost
PGUSER=venueless
PGDATABASE=venueless
PGPASSWORD=hase123
PGPORT=5432

VENUELESS_URL=https://event.kif.rocks/
VENUELESS_WORLD_ID=my-world

SMTP_HOST=mail.my-domain.tld
SMTP_PORT=587
SMTP_USER=my-smtp-user
SMTP_PASSWORD=hase123

EMAIL_FROM=my-email@my-domain.tld
```

### Email templates

Copy the _email-templates.proto_ directory to the name _email-templates_.

Modify the content of the _email-templates/index.json_ to your needs. The schema
is as following:

```typescript
type IndexJSON = IndexJSONEntry[];

interface IndexJSONEntry {
  // The order position item numbers to match
  item: number | number[];
  // The file(s) to use as email template
  template: {
    subject: string;
    text?: string;
    html?: string;
  };
}
```

Create email template files accordingly. They use the
[lodash template syntax](https://lodash.com/docs/4.17.15#template) and have
access to an object of the following interface:

```typescript
interface EmailData {
  // The email address to send this mail to
  email: string;
  // The single ticket data, see below
  tickets: Ticket[];
}

interface Ticket {
  // The access link
  url: string;
  // The order position item ID
  item: number;
  // Key-value mapping of order position questions
  answers: Record<number, string>;
}
```
