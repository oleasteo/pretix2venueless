import { get } from "node:https";

/**
 * @param {string} url
 * @param {RequestOptions=} options
 * @returns {Promise<any>}
 */
export async function fetchJSON(url, options) {
  return await new Promise((resolve, reject) => {
    const req = get(url, options, (res) => {
      let body = "";
      res.on("data", (chunk) => {
        body += chunk;
      });
      res.on("end", () => {
        try {
          resolve(JSON.parse(body));
        } catch (error) {
          reject(error);
        }
      });
    });
    req.once("error", reject);
  });
}
