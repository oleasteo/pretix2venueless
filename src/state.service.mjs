import { fileURLToPath } from "node:url";
import path from "node:path";
import { readFile, writeFile } from "node:fs/promises";

/** @typedef { import('./types').State } State */
/** @typedef { import('./types').StateDTO } StateDTO */

const __filename = fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);

const STATE_FILE = path.resolve(__dirname, "..", ".state.json");

/**
 * @type {State}
 */
export let STATE = await readState();

/**
 * @returns {Promise<void>}
 */
export async function persistState() {
  await writeFile(STATE_FILE, JSON.stringify(stateToDTO(STATE)));
}

/**
 * @returns {Promise<State>}
 */
async function readState() {
  let content;
  try {
    content = await readFile(STATE_FILE);
  } catch (ignored) {
    return { latestDate: null, processedIds: new Set() };
  }
  return stateFromDTO(JSON.parse(content.toString()));
}

/**
 * @param {State} state
 * @returns {StateDTO}
 */
function stateToDTO(state) {
  return {
    processedIds: [...state.processedIds],
    latestDate: state.latestDate?.toISOString(),
  };
}

/**
 * @param {StateDTO} dto
 * @returns {State}
 */
function stateFromDTO(dto) {
  return {
    processedIds: new Set(dto.processedIds),
    latestDate: dto.latestDate ? new Date(dto.latestDate) : null,
  };
}
