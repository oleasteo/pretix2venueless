/** @typedef { import('./types').Ticket } Ticket */

import { customAlphabet } from "nanoid";
import { sign } from "./jwt.mjs";

const SHORT_TOKEN = customAlphabet(
  "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789",
  21
);

/**
 * @param {Ticket} ticket
 */
export async function createTokenEntry(ticket) {
  return {
    id: SHORT_TOKEN(),
    token: await sign({
      uid: ticket.id,
      traits: ["participant"],
      profile: {
        display_name: ticket.answers[process.env.PRETIX_ANSWER_NAME],
      },
    }),
  };
}
