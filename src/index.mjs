#!/usr/bin/env node

import "./00-preflight.mjs";

import _ from "lodash";
import { fetchTickets } from "./pretix.service.mjs";
import { createTokenEntry } from "./venueless.service.mjs";
import { sendMailByTemplate } from "./email.service.mjs";
import { dbClose, writeToken } from "./db.service.mjs";
import { persistState, STATE } from "./state.service.mjs";
import { DRY_VENUELESS } from "./dry.service.mjs";

run().catch(console.error);

async function run() {
  const tickets = await fetchTickets();
  const entries = await Promise.all(
    tickets.map(async (ticket) => {
      const { id, token } = await createTokenEntry(ticket);
      await writeToken(id, token);
      console.debug(
        `${
          DRY_VENUELESS ? "[DRY] " : ""
        }token ${id} written for OrderPosition#${ticket.id}: ${token}`
      );
      return {
        url: `${process.env.VENUELESS_URL}login/${id}`,
        ...ticket,
      };
    })
  );
  const recipients = Object.entries(_.groupBy(entries, "email")).map(
    ([email, tickets]) => ({
      datetime: _.max(tickets.map((it) => it.datetime)),
      email,
      tickets: tickets.map(({ id, url, item, answers }) => ({
        id,
        url,
        item,
        answers,
      })),
    })
  );
  await Promise.all(
    recipients.map(async (it) => {
      await sendMailByTemplate(it);
      if (STATE.latestDate === null || it.datetime > STATE.latestDate) {
        STATE.latestDate = it.datetime;
      }
    })
  );
  STATE.latestDate = STATE.latestDate ? new Date(STATE.latestDate) : null;
  STATE.latestDate?.setMilliseconds(0);
  recipients.forEach((it) =>
    it.tickets.forEach((it) => STATE.processedIds.add(it.id))
  );
  await dbClose();
  await persistState();
}
