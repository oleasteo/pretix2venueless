export interface OrderPositionDTO {
  id: number;
  item: number;
  attendee_email: string | null;
  answers?: OrderPositionAnswerDTO[];
  canceled: boolean;

  // ... irrelevant fields omitted
}

export interface OrderDTO {
  code: string;
  email: string;
  datetime: string;
  positions: OrderPositionDTO[];

  // ... irrelevant fields omitted
}

export interface OrderPositionAnswerDTO {
  question: number;
  answer: string;

  // ... irrelevant fields omitted
}

export interface Ticket {
  id: OrderPositionDTO["id"];
  item: OrderPositionDTO["item"];
  datetime: Date;
  email: string;
  answers: Record<number, string>;
}

export interface EmailData {
  datetime: Date;
  // The email address to send this mail to
  email: string;
  // The single ticket data, see below
  tickets: EmailTicketData[];
}

interface EmailTicketData {
  // The access link
  url: string;
  // The order position item ID
  item: number;
  // Key-value mapping of order position questions
  answers: Record<number, string>;
}

export interface State {
  latestDate: Date | null;
  // @ts-ignore
  processedIds: Set<number>;
}

export interface StateDTO {
  latestDate: string;
  processedIds: number[];
}
